#!/bin/bash

echo "[TASK 1] Install containerd runtime"
# Installer Containerd
## Prérequis
modprobe overlay
modprobe br_netfilter

## Installation de Containerd
apt-get update && apt-get install -y containerd

## Configurer Containerd
mkdir -p /etc/containerd
containerd config default > /etc/containerd/config.toml

## Redémarrer Containerd
systemctl restart containerd

echo "[TASK 2] Install Kubernetes components (kubeadm, kubelet and kubectl)"

# Installer kubeadm, kubelet et kubectl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl
